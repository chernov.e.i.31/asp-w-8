import React from "react";
import { Component } from "react";
import { LabelWithInputProps } from "../Model/LabelWithInputProps";
  
export default class LabelWithInput extends Component<LabelWithInputProps> {
    render() {
      return <>
        <div className="row justify-content-center">
            <div className="col-4">
                <label>{this.props.labelText}</label>
            </div>
            <div className="col-4">
                <input type={this.props.inputType} onChange={this.props.onChange}></input>
            </div>
        </div>
      </>
    }
}
import React, { Component, useState } from "react";
import ButtonAction from '../Components/Button.tsx';
import LabelWithInput from '../Components/LabelWithInput.tsx';
import { RegistrationState } from "../Model/RegistrationState.tsx";
import { useDispatch } from "react-redux";
import { Actions, User } from "../Reducers/userReducer.ts";
import NewUser from "./NewUser.tsx";

const Registration = (props:RegistrationState, user:User) => {
    const [name, setName] = useState(props.name);
    const [surname, setSurName] = useState(props.name);
    const [password, setPassword] = useState(props.password);
    const [passwordConfirm, setPasswordConfirm] = useState(props.passwordConfirm);
    const [result, setResult] = useState(props.result);
    user = {fullName: ""};

    const dispatch = useDispatch();

    const add = () => {
        user.fullName= name + " " + surname;    
        dispatch(Actions.addUser( user));
    };

    const onClick = (e: any) => {
        setResult("");
        if(password === passwordConfirm)
        {
            fetch('/users/add', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    username: name,
                    lastName: surname,
                    password: password,
                })
            })
            .then(res => {
                if (res.status === 200)
                {
                    add();
                }
                else
                {
                    setResult("Error");
                }
                return res.json();
            } )
            .then(function (data) {
                console.log('Request succeeded with JSON response', data);
                })
            .catch(function (error) {
                console.log('Request failed', error);
            });
        }
        else
        {
            setResult("Password is not equal");
        }
    }

    return <>
        <LabelWithInput labelText="Name" inputType="text" onChange={(e)=>setName(e.target.value)} />
        <LabelWithInput labelText="SurName" inputType="text" onChange={(e)=>setSurName(e.target.value)} />
        <LabelWithInput labelText="Password" inputType="password" onChange={(e)=>setPassword(e.target.value)} />
        <LabelWithInput labelText="Password" inputType="password" onChange={(e)=>setPasswordConfirm(e.target.value)} />
        <ButtonAction text="Зарегистрировать" onClick={onClick} />
        {result}
        <NewUser />
    </>;
}
export default Registration;
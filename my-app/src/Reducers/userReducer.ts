

export const UserState = Object.freeze({
    USER_ADD: '[USER_STATE] USER_ADD',
});

export interface User {
    fullName: string;
}

interface State {
    user: User|null;
}

let initialState: State = {
    user: null,
}

export const Actions = Object.freeze({
    addUser: (user: User) => ({ type: UserState.USER_ADD, payload: user })
});

const userReducer = (state  = initialState, action: any)=>
{
    switch (action.type){
        case UserState.USER_ADD:
            return { ...state, user: action.payload };
        default :
            return state;
    }
}

export default userReducer;
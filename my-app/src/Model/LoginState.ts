export interface LoginState {
    name: string;
    password: string;
    result: any;
}